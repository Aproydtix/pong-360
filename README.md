# Pong 360
Made in Unity using C#.

First game project at university, a re-imagining of Pong with a circular arena.
Players can only move along the circular path, the last player to touch the ball gains a point if it leaves the arena.
Additional features such as 2-4 players, AI, and two graphical modes have been added.
These can be accessed by opening the UI in the upper left, or with dev keys.

If I had worked on it more I would have added AI difficulties, improved trail and particle effects, and reworked the collision system so the ball wouldn't be able to pass through the paddles at higher speeds.

This was a group project, but the idea was mine, and the entire implementation was done by me. 

## Controls
- Escape: Pauses the game
- Q: Changes graphical mode
- R: Restart
- 2-4: Changes the amount of players
### Players
- A/D: Player 1 controls
- Left/Right: Player 2 controls
- F/H: Player 3 controls
- J/L: Player 4 controls

Gamepads are also supported for player input

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/Wfm6L1SFFWE

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
