﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class PaddleScript : MonoBehaviour {

    ControllerScript controllerScript;
    public int player;
    KeyCode clockWise;
    KeyCode counterClockWise;
    float speed;
    GamePad.Index gamePadIndex;
    bool freeGamePadMovement = false;
    public bool isAI = false;
    public bool AICalculated = false;
    float currentAngle;
    float goalAngle;

    // Use this for initialization
    void Start ()
    {
        controllerScript = GameObject.Find("Controller").GetComponent<ControllerScript>();
        freeGamePadMovement = controllerScript.freeGamePadMovement;

        //Player input
        if (player == 0)
        {
            clockWise = KeyCode.D;
            counterClockWise = KeyCode.A;
            gamePadIndex = GamePad.Index.One;
        }
        if (player == 1)
        {
            clockWise = KeyCode.RightArrow;
            counterClockWise = KeyCode.LeftArrow;
            gamePadIndex = GamePad.Index.Two;
        }
        if (player == 2)
        {
            clockWise = KeyCode.H;
            counterClockWise = KeyCode.F;
            gamePadIndex = GamePad.Index.Three;
        }
        if (player == 3)
        {
            clockWise = KeyCode.L;
            counterClockWise = KeyCode.J;
            gamePadIndex = GamePad.Index.Four;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (controllerScript.winner >= 0)
        {
            return;
        }

        if (player < controllerScript.playerCount)
        {
            speed = controllerScript.speed / 3f; //Set according to global speed

            if (controllerScript.ball.GetComponent<BallScript>().waitTimer > 0)
                AICalculated = false;

            int direction = 0;
            if (Input.GetKey(clockWise))
                direction += 1;
            if (Input.GetKey(counterClockWise))
                direction -= 1;

            if (!isAI)
            {
                if (GamePad.GetAxis(GamePad.Axis.LeftStick, gamePadIndex).magnitude < .1f)
                    transform.RotateAround(Vector3.zero, Vector3.forward, -direction * speed * Time.deltaTime * 60f);
                else
                    GamePadInput();

            }
            else
            {
                Vector3 ballPos = controllerScript.ball.transform.position;
                Vector3 ballDir = controllerScript.ball.GetComponent<Rigidbody2D>().velocity;
                Vector3 radius = transform.position;

                float ballDistanceToIntersect = 0;
                //if (!AICalculated)
                {
                    if (controllerScript.ball.GetComponent<BallScript>().activePlayer != player)
                    {
                        ballDistanceToIntersect = 
                            (2 * ballPos.magnitude * Mathf.Cos(Vector3.Angle(ballDir, ballPos))                                      // (- b
                            + Mathf.Sqrt(                                                                                               // + SquareRoot of (
                                Mathf.Pow(2 * ballPos.magnitude * Mathf.Cos(Vector3.Angle(ballDir, ballPos) * Mathf.Deg2Rad), 2)     // b^2
                                - 4 * (Mathf.Pow(ballPos.magnitude, 2) - Mathf.Pow(radius.magnitude, 2))))                            // -4ac)
                                / 2f;                                                                                                   // )/2a
                        goalAngle = Vector2.Angle(Vector2.right, ballPos + ballDir.normalized * ballDistanceToIntersect);
                        if ((ballPos + ballDir.normalized * ballDistanceToIntersect).y < 0) goalAngle = -goalAngle;
                    }
                }

                currentAngle = Vector2.Angle(Vector2.right, transform.position);
                if (transform.position.y < 0) currentAngle = -currentAngle;
                if (controllerScript.ball.GetComponent<BallScript>().activePlayer == player)
                    goalAngle = currentAngle;

                    float angle = (goalAngle - currentAngle);
                if (Mathf.Abs(goalAngle - currentAngle) > 180) angle *= -1;
                if (Mathf.Abs(angle) > speed) angle = speed * Mathf.Sign(angle);

                if (Mathf.Abs(angle) > 1f)
                    transform.RotateAround(Vector3.zero, Vector3.forward, angle * Time.deltaTime * 60f);
                else
                    AICalculated = true;
            }
        }
    }

    void GamePadInput()
    {
        currentAngle = Vector2.Angle(Vector2.right, transform.position);
        if (transform.position.y < 0) currentAngle = -currentAngle;
        goalAngle = Vector2.Angle(Vector2.right, GamePad.GetAxis(GamePad.Axis.LeftStick, gamePadIndex));
        if (GamePad.GetAxis(GamePad.Axis.LeftStick, gamePadIndex).y < 0) goalAngle = -goalAngle;
        float angle = (goalAngle - currentAngle);
        if (Mathf.Abs(goalAngle - currentAngle) > 180) angle *= -1;
        if (!freeGamePadMovement)
            if (Mathf.Abs(angle) > speed) angle = speed * Mathf.Sign(angle);

        if (Mathf.Abs(angle) > 1f)
            transform.RotateAround(Vector3.zero, Vector3.forward, angle * Time.deltaTime*60f);
    }
}
