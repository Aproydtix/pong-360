﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

    ControllerScript controllerScript;
    public int activePlayer;
    float speed;
    public int bounceCount = 0;
    public float waitTimer = 1f;
    bool directionSet = false;
    bool active = true;

	// Use this for initialization
	void Start ()
    {
        controllerScript = GameObject.Find("Controller").GetComponent<ControllerScript>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (controllerScript.winner >= 0)
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }

        while (activePlayer >= controllerScript.playerCount)
            activePlayer = Random.Range(0, controllerScript.playerCount);

        GetComponent<SpriteRenderer>().color = controllerScript.playerColor[activePlayer];
        GetComponent<TrailRenderer>().material.color = controllerScript.playerColor[activePlayer];
        speed = controllerScript.speed;

        if (controllerScript.fancy)
        {
            if (GetComponent<SpriteRenderer>().sprite != Resources.Load<Sprite>("Sprites/BallFancy"))
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/BallFancy");
            if (waitTimer <= 0)
                GetComponent<TrailRenderer>().enabled = true;
        }
        else
        {
            if (GetComponent<SpriteRenderer>().sprite != Resources.Load<Sprite>("Sprites/Ball"))
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Ball");
            GetComponent<TrailRenderer>().enabled = false;
        }

        //Initial direction of the ball with a minor wait
        if (directionSet == false)
        {
            waitTimer -= Time.deltaTime;
            if (waitTimer < 0)
            {
                directionSet = true;
                NewDirection(true, Vector3.zero);
                active = true;
            }
        }

        //Point scoring and ball reset
        if (Vector3.Distance(Vector3.zero, transform.position) > 5.5f)
        {
            
                controllerScript.ParticlesExplosion(transform.position, activePlayer);
                controllerScript.ParticlesExplosion(
                    Camera.main.ScreenToWorldPoint(
                        new Vector3(controllerScript.scoreDisplay[activePlayer].transform.position.x * Screen.width, controllerScript.scoreDisplay[activePlayer].transform.position.y * Screen.height, 0)),
                    activePlayer);
            controllerScript.screenShakeAmount = .1f;
            controllerScript.screenShakeTimer = .4f;
            if (controllerScript.fancy)
            {
                GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/ScoreFancy");
                GetComponent<TrailRenderer>().enabled = false;
            }
            else
            {
                GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/Score");
            }
            GetComponent<AudioSource>().Play();
            waitTimer = .5f;
            directionSet = false;
            controllerScript.score[activePlayer] += 1;
            transform.position = Vector3.zero;
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            bounceCount = 0;
        }
	}

    //Paddle collision
    void OnTriggerEnter2D(Collider2D other)
    {
        if (active == false)
            return;
        if (other.gameObject.name.Contains("Paddle"))
        {
            if (other.gameObject.GetComponent<PaddleScript>().player != activePlayer)
            {
                activePlayer = other.gameObject.GetComponent<PaddleScript>().player;
                NewDirection(false, transform.position);
                    controllerScript.ParticlesExplosion(transform.position, activePlayer);
                    bounceCount++;
                controllerScript.screenShakeAmount = .05f;
                controllerScript.screenShakeTimer = .05f;
                if (controllerScript.fancy)
                    GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/HitFancy");
                else
                    GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/Hit");
                GetComponent<AudioSource>().Play();
            }
        }
    }

    //Avoids multiple collisions with players using active
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("Paddle"))
        {
            if (other.gameObject.GetComponent<PaddleScript>().player == activePlayer)
            {
                active = true;
            }
        }
    }

    //Direction
    void NewDirection(bool initial, Vector3 hitPoint)
    {
        float randomness = 2f;
        if (initial)
        {
            //Initial dircetion of the ball is towards players that are not the active player
            int moveTowardsPlayer = Random.Range(0, controllerScript.playerCount);
            while (moveTowardsPlayer == activePlayer)
                moveTowardsPlayer = Random.Range(0, controllerScript.playerCount);
            GetComponent<Rigidbody2D>().velocity =
                (//-new Vector3(Random.Range(-randomness, randomness), +Random.Range(-randomness, randomness), 0)
                new Vector3(
                    controllerScript.paddle[moveTowardsPlayer].transform.position.x,
                    controllerScript.paddle[moveTowardsPlayer].transform.position.y,
                    0)).normalized * speed;
        }
        else
        {
            //The ball moves according to it's position from the paddle
            Vector3 ballPos = new Vector3(hitPoint.x, hitPoint.y, 0);
            Vector3 paddlePos = new Vector3(controllerScript.paddle[activePlayer].transform.position.x, controllerScript.paddle[activePlayer].transform.position.y, 0);

            GetComponent<Rigidbody2D>().velocity =
                (((ballPos - paddlePos)/2f).normalized*2 + (Vector3.zero -
                ballPos).normalized*3).normalized * speed;

            
            /*GetComponent<Rigidbody2D>().velocity =
                (new Vector3(Random.Range(-randomness, randomness), +Random.Range(-randomness, randomness), 0)
                - new Vector3(
                    controllerScript.paddle[activePlayer].transform.position.x,
                    controllerScript.paddle[activePlayer].transform.position.y,
                    0)).normalized * speed;*/ //Old random position
        }
        for (int i = 0; i < 4; i++)
        { controllerScript.paddle[i].GetComponent<PaddleScript>().AICalculated = false; }
        active = false;
    }
}
