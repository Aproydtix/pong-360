﻿using UnityEngine;
using System.Collections;

public class ParticleScript : MonoBehaviour
{
    ControllerScript controllerScript;

    public float r = .8f;
    public float g = .8f;
    public float b = .8f;
    public float startAlpha = 1f;
    public float alpha;
    public float xScroll = .05f;
    public float yScroll = .05f;
    public float xScale = 1;
    public float yScale = 1;
    public float xScaleChange = 1;
    public float yScaleChange = 1;
    float xScrolling;
    float yScrolling;
    float zScrolling;
    public float duration = 1.5f; // time to die
    float curDur;
    public Vector3 target;
    public Sprite sprite;
    public bool fade;
    public float waitBeforeFade;
    float fadeWait;
    public float rotation;
    public float yDis = 0;

    // Use this for initialization
    public void Start()
    {
        controllerScript = GameObject.Find("Controller").GetComponent<ControllerScript>();

        if (GetComponent<SpriteRenderer>() == null)
            gameObject.AddComponent<SpriteRenderer>();
        GetComponent<SpriteRenderer>().sprite = sprite;
        GetComponent<SpriteRenderer>().material.color = new Color(r, g, b, startAlpha); // set text color
        transform.eulerAngles = new Vector3(0, rotation, 0);
        transform.position = new Vector3(target.x, target.y, target.z);
        transform.localScale = new Vector3(xScale, yScale, xScale);
        xScrolling = 0;
        yScrolling = 0;
        zScrolling = 0;
        curDur = 0;
        fadeWait = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (controllerScript.winner >= 0)
            return;

        if (controllerScript.fancy)
            GetComponent<SpriteRenderer>().enabled = true;
        else
            GetComponent<SpriteRenderer>().enabled = false;

        if (duration <= curDur)
            return;
        transform.position = new Vector3
            (
            target.x + xScrolling,
            target.y + yScrolling,
            0
            );
        if (fadeWait > waitBeforeFade)
        {
            if (fade) { alpha = startAlpha * (1 - curDur / duration); }
            else { alpha = startAlpha; }
            curDur += Time.deltaTime;
            GetComponent<SpriteRenderer>().material.color = new Color(r, g, b, alpha);
            transform.localScale = new Vector3
            (
            transform.localScale.x + xScaleChange,
            transform.localScale.y + yScaleChange,
            transform.localScale.z
            );
        }
        xScrolling += (xScroll * Time.deltaTime) / 10f;
        yScrolling += (yScroll * Time.deltaTime) / 10f;

        fadeWait += Time.deltaTime;
    }

    void LateUpdate()
    {
        if ((duration <= curDur)
            ||
            ((transform.localScale.x < 0 || transform.localScale.y < 0) && xScaleChange < 0)
            ||
            ((transform.localScale.x > 0 || transform.localScale.y > 0) && xScaleChange > 0))
        { Destroy(gameObject); }
    }
}
