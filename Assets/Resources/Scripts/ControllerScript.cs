﻿using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour {

    public int playerCount = 2;
    public float baseSpeed = 10f; 
    public float speed; //speed modifier for ball and paddles
    public int[] score = new int[4];
    public bool fancy = false;
    public bool freeGamePadMovement = false;
    public int endState = 10;
    public int winner = -1;
    bool winnerSet = false;
    GameObject[] winscreen = new GameObject[3];
    GameObject filter;
    GameObject pauseText;

    GameObject ring; //only for visual feedback
    public GameObject ball;
    public GameObject[] paddle;
    Color[] originalColor = new Color[4];
    public Color[] playerColor = new Color[4];
    public GameObject[] scoreDisplay;
    GameObject background;

    public Sprite[] particles;
    public float screenShakeTimer = 0;
    public float screenShakeAmount = .1f;

    bool show = false;

    // Use this for initialization
    void Start()
    {
        playerCount = Mathf.Clamp(playerCount, 2, 4);
        speed = baseSpeed;

        ring = Instantiate(Resources.Load<GameObject>("GameObjects/Ring"));
        ring.transform.position= new Vector3(0, 0, 1);

        paddle = new GameObject[4];
        scoreDisplay = new GameObject[4];
        originalColor[0] = new Color(.85f, .25f, .25f, 1f); //Red
        originalColor[1] = new Color(.25f, .25f, .85f, 1f); //Blue
        originalColor[2] = new Color(.25f, .85f, .25f, 1f); //Green
        originalColor[3] = new Color(.75f, .75f, .25f, 1f); //Yellow

        for (int i = 0; i < paddle.Length; i++)
        {
            playerColor[i] = originalColor[i];
            //Creating players
            paddle[i] = Instantiate(Resources.Load<GameObject>("GameObjects/Paddle"));
            paddle[i].name = "Paddle " + (i+1).ToString();
            paddle[i].transform.position = new Vector3( //Evenly spaces players around the centre
                Mathf.Cos(Mathf.PI+Mathf.PI*i*2 / (playerCount))*4.25f,
                Mathf.Sin(Mathf.PI+Mathf.PI*i*2 / (playerCount))*4.25f,
                0);
            paddle[i].transform.eulerAngles = new Vector3( //Rotates paddle to align with ring
                0,
                0,
                360 * i / playerCount + 269);
            paddle[i].GetComponent<SpriteRenderer>().color = playerColor[i];
            paddle[i].GetComponent<PaddleScript>().player = i;
            scoreDisplay[i] = Instantiate(Resources.Load<GameObject>("GameObjects/ScoreDisplay"));
            scoreDisplay[i].name = "ScoreDisplay Player " + (i + 1).ToString();
            scoreDisplay[i].GetComponent<GUIText>().color = playerColor[i];
            if (i == 0)
                scoreDisplay[i].transform.position = new Vector3(.15f, .75f, 0f); //Upper Right
            if (i == 1)
                scoreDisplay[i].transform.position = new Vector3(.85f, .75f, 0f); //Upper Left
            if (i == 2)
                scoreDisplay[i].transform.position = new Vector3(.15f, .25f, 0f); //Lower Left
            if (i == 3)
                scoreDisplay[i].transform.position = new Vector3(.85f, .25f, 0f); //Lower Right
            if (i >= playerCount)
            {
                paddle[i].gameObject.SetActive(false);
                scoreDisplay[i].gameObject.SetActive(false);
            }
        }

        ball = Instantiate(Resources.Load<GameObject>("GameObjects/Ball"));
        ball.GetComponent<BallScript>().activePlayer = Random.Range(0,playerCount);

        background = new GameObject();
        background.name = "Background";
        background.transform.localScale = new Vector3(1.2f, 1, 1);
        background.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Background");
        if (fancy)
            background.GetComponent<SpriteRenderer>().enabled = true;
        else
            background.GetComponent<SpriteRenderer>().enabled = false;
        background.transform.position = new Vector3(0,0,10);
        background.GetComponent<SpriteRenderer>().material.color = new Color(.2f,.2f,.2f,1f);

        filter = new GameObject();
        filter.name = "Filter";
        filter.transform.position = new Vector3(0, 0, -5);
        filter.transform.localScale = Vector3.one * 1000;
        filter.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Pixel");
        filter.GetComponent<SpriteRenderer>().material.color = Color.clear;
        pauseText = Instantiate(Resources.Load<GameObject>("GameObjects/ScoreDisplay"));
        pauseText.name = "Pause Text";
        pauseText.GetComponent<GUIText>().text = "\n\nPaused\n\n\n\n\nDevKeys: Q,2,3,4\nPress E to Exit";
        pauseText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Devkeys and shortcuts
        if (Input.GetKeyDown(KeyCode.R))
        { Application.LoadLevel(0); Time.timeScale = 1; }
        if (Input.GetKeyDown(KeyCode.Q))
            fancy = !fancy;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            playerCount = 2;
        if (Input.GetKeyDown(KeyCode.Alpha3))
            playerCount = 3;
        if (Input.GetKeyDown(KeyCode.Alpha4))
            playerCount = 4;
        //Pause
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale != 0)
            {
                Time.timeScale = 0;
                pauseText.gameObject.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                pauseText.gameObject.SetActive(false);
            }
        }
        if (Time.timeScale == 0)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Application.Quit();
#if UNITYEDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
            }
        }

        //Winner
        if (winner >= 0)
        {
            if (winnerSet == false)
            {
                winscreen[0] = Instantiate(Resources.Load<GameObject>("GameObjects/ScoreDisplay"));
                winscreen[0].name = "Winscreen 1";
                winscreen[0].GetComponent<GUIText>().fontSize = 128;
                winscreen[0].GetComponent<GUIText>().color = originalColor[winner];
                winscreen[0].GetComponent<GUIText>().text = "Player " + (winner+1).ToString() + "\n\n";

                winscreen[1] = Instantiate(Resources.Load<GameObject>("GameObjects/ScoreDisplay"));
                winscreen[1].name = "Winscreen 2";
                winscreen[1].GetComponent<GUIText>().fontSize = 128;
                winscreen[1].GetComponent<GUIText>().text = "\nWins!\n";

                winscreen[2] = Instantiate(Resources.Load<GameObject>("GameObjects/ScoreDisplay"));
                winscreen[2].name = "Winscreen 3";
                winscreen[2].GetComponent<GUIText>().fontSize = 64;
                winscreen[2].GetComponent<GUIText>().text = "\n\n\n\nPress R to restart";
                winnerSet = true;

                if (fancy)
                    for (int i = 0; i < 3; i++) winscreen[i].GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/Montserrat-Regular");
                else
                    for (int i = 0; i < 3; i++) winscreen[i].GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/arial");
            }
        }
        if (Time.timeScale == 0 || winner >= 0)
        {
            filter.GetComponent<SpriteRenderer>().material.color = new Color(0, 0, 0, .5f);
            if (fancy)
                pauseText.GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/Montserrat-Regular");
            else
                pauseText.GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/arial");
        }
        else
            filter.GetComponent<SpriteRenderer>().material.color = Color.clear;

        if (fancy)
        {
            ring.GetComponent<SpriteRenderer>().color = new Color(
                .9f * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                .9f * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                .9f * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                .15f);
            if (ring.GetComponent<SpriteRenderer>().sprite != Resources.Load<Sprite>("Sprites/RingFancy"))
                ring.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/RingFancy");
            if (!Camera.main.GetComponent<AudioSource>().isPlaying)
            { Camera.main.GetComponent<AudioSource>().Play(); }
            background.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            ring.GetComponent<SpriteRenderer>().color = Color.white;
            if (ring.GetComponent<SpriteRenderer>().sprite != Resources.Load<Sprite>("Sprites/Ring"))
                ring.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Ring");
            if (Camera.main.GetComponent<AudioSource>().isPlaying)
            { Camera.main.GetComponent<AudioSource>().Stop(); }
            background.GetComponent<SpriteRenderer>().enabled = false;
        }
        speed = baseSpeed * (1 + ball.GetComponent<BallScript>().bounceCount / 50f);

        for (int i = 0; i < paddle.Length; i++)
        {
                paddle[i].gameObject.SetActive(true);
                scoreDisplay[i].gameObject.SetActive(true);

            if (fancy)
            {
                if (paddle[i].GetComponent<SpriteRenderer>().sprite != Resources.Load<Sprite>("Sprites/PaddleFancy"))
                    paddle[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/PaddleFancy");
                playerColor[i] = new Color(
                    originalColor[i].r * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                    originalColor[i].g * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                    originalColor[i].b * (Mathf.Cos(Time.timeSinceLevelLoad * 10f) / 6f + 1f),
                    1f);
            }
            else
            {
                paddle[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Paddle");
                playerColor[i] = originalColor[i];
            }
            scoreDisplay[i].GetComponent<GUIText>().color = playerColor[i];
            //If the paddle is the active player it becomes transparent and is set on a different z value
            if (i == ball.GetComponent<BallScript>().activePlayer)
            {
                paddle[i].GetComponent<SpriteRenderer>().color = new Color(playerColor[i].r, playerColor[i].g, playerColor[i].b, .5f);
                paddle[i].transform.position = new Vector3(paddle[i].transform.position.x, paddle[i].transform.position.y, 0);
            }
            else
            {
                paddle[i].GetComponent<SpriteRenderer>().color = playerColor[i];
                paddle[i].transform.position = new Vector3(paddle[i].transform.position.x, paddle[i].transform.position.y, -1);
            }

            scoreDisplay[i].GetComponent<GUIText>().text = score[i].ToString();
            if (fancy)
                scoreDisplay[i].GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/Montserrat-Regular");
            else
                scoreDisplay[i].GetComponent<GUIText>().font = Resources.Load<Font>("Fonts/arial");

            if (i >= playerCount)
            {
                paddle[i].gameObject.SetActive(false);
                scoreDisplay[i].gameObject.SetActive(false);
            }

            if (score[i] >= endState)
                winner = i;
        }

        //Screen Shake
        if (screenShakeTimer > 0 && Time.timeScale != 0)
        {
            screenShakeTimer-=Time.deltaTime;
            if (fancy)
                Camera.main.transform.position = new Vector3(Random.Range(-screenShakeAmount, screenShakeAmount), Random.Range(-screenShakeAmount, screenShakeAmount), Camera.main.transform.position.z);
        }
        if (!(screenShakeTimer > 0 && Time.timeScale != 0) || !fancy)
        {
            Camera.main.transform.position = new Vector3(0,0, Camera.main.transform.position.z);
            if (Time.timeScale != 0 && fancy)
                screenShakeTimer = 0;
        }
    }

    public void ParticlesExplosion(Vector3 position, int player)
    {
        SpawnParticles(position, player, 10f, 10f, 30, .5f);
    }

    void SpawnParticles(Vector3 position, int player, float xScroll, float yScroll, int amount, float scale)
    {
        for (int i = 0; i < amount; i++)
        {
            float ranDir = Random.Range(-2 * Mathf.PI, 2 * Mathf.PI);
            GameObject particle = new GameObject();
            particle.name = "Particle";
            particle.AddComponent<ParticleScript>();
            particle.GetComponent<ParticleScript>().r = playerColor[player].r-.2f;
            particle.GetComponent<ParticleScript>().g = playerColor[player].g-.2f;
            particle.GetComponent<ParticleScript>().b = playerColor[player].b-.2f;
            particle.GetComponent<ParticleScript>().alpha = playerColor[player].a-.2f;
            particle.GetComponent<ParticleScript>().fade = true;
            particle.GetComponent<ParticleScript>().xScroll = Mathf.Cos(ranDir)*xScroll;
            particle.GetComponent<ParticleScript>().yScroll = Mathf.Sin(ranDir)*yScroll;
            particle.GetComponent<ParticleScript>().xScale = Random.Range(.15f, .2f) * scale;
            particle.GetComponent<ParticleScript>().yScale = Random.Range(.15f, .2f) * scale;
            particle.GetComponent<ParticleScript>().xScaleChange = Random.Range(-.0005f, -.0001f) * scale;
            particle.GetComponent<ParticleScript>().yScaleChange = Random.Range(-.0005f, -.0001f) * scale;
            particle.GetComponent<ParticleScript>().rotation = Random.Range(-180, 180);
            particle.GetComponent<ParticleScript>().duration = 1f;
            particle.GetComponent<ParticleScript>().waitBeforeFade = 0f;
            particle.GetComponent<ParticleScript>().sprite = Resources.Load<Sprite>("Sprites/BallFancy"); //particles[Random.Range(0, particles.Length)];
            particle.GetComponent<ParticleScript>().target = position;
        }
    }

    public Texture btnTexture;
    void OnGUI()
    {
        if (!btnTexture)
        {
            Debug.LogError("Please assign a texture on the inspector");
            return;
        }

        if (!show)
        {
            GUI.color = Color.grey;
            if (GUI.Button(new Rect(10, 10, 50, 30), "Show"))
                show = true;
        }

        if (show)
        {
            GUI.color = Color.white;
            if (GUI.Button(new Rect(10, 10, 50, 30), "Hide"))
                show = false;

            if (!fancy) GUI.color = Color.grey;
            if (GUI.Button(new Rect(10, 50, 50, 30), "HQ"))
                fancy = !fancy;

            for (int i = 0; i < playerCount; i++)
            {
                GUI.color = originalColor[i];
                if (GUI.Button(new Rect(20, 90 + i * 50, 60, 40), "Player " + (i+1).ToString()))
                    ;

                GUI.color = Color.white;
                if (i > 1 && i == playerCount-1)
                    if (GUI.Button(new Rect(0, 90 + i*50, 20, 20), "-"))
                        playerCount -= 1;

                if (i < 3 && i == playerCount-1)
                    if (GUI.Button(new Rect(0, 110 + i*50, 20, 20), "+"))
                        playerCount += 1;

                if (!paddle[i].GetComponent<PaddleScript>().isAI) GUI.color = Color.grey;
                if (GUI.Button(new Rect(80, 90 + i * 50, 30, 40), "AI"))
                    paddle[i].GetComponent<PaddleScript>().isAI = !paddle[i].GetComponent<PaddleScript>().isAI;

                playerCount = Mathf.Clamp(playerCount, 2, 4);
            }


        }

    }
}
